<?php

return [
    /*
     * Database config
     */
    'db' => [
        'dsn' => 'mysql:host=localhost;dbname=phones',
        'username' => 'root',
        'password' => 'MySQ1'
    ],

    /*
     * Parser config
     */
    'parser' => [
        'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36',
    ],

    /*
     * Translator config
     */
    'translator' => [
        'limit' => 30,
        'keys_to_exclude' => [
            'ID', 'ItemID', 'Brand',
        ],
        'yandex' => [
            'api_key' => 'trnsl.1.1.20151009T153357Z.97f0f10c7913eabc.beb1616dfda0a8f22cb90cad4b092048b926950d',
        ],
        'bing' => [
            'client_id' => 'MyTestTranslatorApp001',
            'client_secret' => 'DfHcJ4wBD29EaBds2U/9hCnf3ESCSP6DPucn2QxRMwQ=',
        ],
    ],

    /*
     * Checker config
     */
    'checker' => [
        'limit' => 50,
    ],

    /*
     * Review finder config
     */
    'review_finder' => [
        'limit' => 50,
        'key' => 'AIzaSyBHJ_P7uYQ84dTAgEd14o_oFK3VuYambeE',
        'relevanceLanguage' => 'en',
        'regionCode' => 'US',
    ],

    /*
     * Targets
     */
    'targets' => [
        'smartphones' => require_once(__DIR__ . '/targets/smartphones.php'),
        'notebooks' => require_once(__DIR__ . '/targets/notebooks.php'),
        'tablets' => require_once(__DIR__ . '/targets/tablets.php'),
    ],

];