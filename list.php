<?php
header('Content-Type: text/html; charset=utf8');

$table = isset($_GET['table']) ? filter_var($_GET['table'], FILTER_SANITIZE_STRING) : 'phones_ru';
$page = isset($_GET['page']) ? filter_var($_GET['page'], FILTER_SANITIZE_NUMBER_INT) : 0;

/*
 * Load config and connect to the database
 */
$params = require_once(__DIR__ . '/config.php');
$pdo = new PDO(
    $params['db']['dsn'], $params['db']['username'], $params['db']['password'],
    [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `utf8`',
    ]
);

/*
 * Select tables list
 */
$statement = $pdo->query('SHOW TABLES');
if ($statement->rowCount() == 0) {
    die('There is no tables in database "' . $params['db']['dsn'] . '"');
}
$availableTables = [];
foreach ($statement->fetchAll() as $row) {
    $availableTables[] = $row[0];
}
unset($statement);

if (!in_array($table, $availableTables)) {
    die("Table \"$table\" is not in available table list");
}

/*
 * Select data from a specified table
 */
$limit = 50;
$totalCount = (int)$pdo->query("SELECT COUNT(*) FROM `{$table}`")->fetchColumn();
$pagesCount = ceil($totalCount / $limit);

$offset = $limit * $page;
$statement = $pdo->prepare(
    "SELECT `ID`, `Brand`, `Model` FROM `{$table}` LIMIT $offset,$limit"
);
$statement->execute();
?>
<!DOCTYPE html>
<html>
<head>
    <title>List</title>
    <meta http-equiv="content-type" content="text/html; charset=utf8">
</head>
<body>
<h1>List</h1>
<h2>Available tables:</h2>
<ul>
    <?php foreach ($availableTables as $t): ?>
        <li><a href="/list.php?table=<?= $t ?>"><?= $t ?></a><?php if ($t == $table): ?> <span style="color:red;">*</span><?php endif; ?></li>
    <?php endforeach; ?>
</ul>
<h2>
    Data:
</h2>
<?php if ($statement->rowCount() === 0): ?>
    <h1>No items!</h1>
<?php else: ?>
    <ul>
        <?php foreach ($statement->fetchAll(PDO::FETCH_OBJ) as $item): ?>
            <li>
                <a href="/details.php?id=<?= $item->ID ?>&table=<?= $table ?>"><?= $item->Brand ?> <?= $item->Model ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<?php if ($pagesCount > 0): ?>
<ul style="list-style: none;">
    <?php for ($i = 0; $i < $pagesCount; ++$i): ?>
        <li style="float:left;margin-left:4px;"><a<?php if ($page == $i): ?> style="color:red;"<?php endif; ?> href="/list.php?table=<?= $table ?>&page=<?= $i ?>"><?= $i ?></a></li>
    <?php endfor; ?>
</ul>
<?php endif; ?>
</body>
</html>
