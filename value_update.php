<?php
$required = [
    'table' => FILTER_SANITIZE_STRING,
    'field' => FILTER_SANITIZE_STRING,
    'id' => FILTER_SANITIZE_NUMBER_INT,
    'value' => FILTER_SANITIZE_STRING,
];
foreach (array_keys($required) as $field) {
    if (!isset($_POST[$field])) {
        die("Required param \"$field\" is missing");
    }
}

$config = require_once(__DIR__ . '/config.php');

try {
    $pdo = new PDO(
        $config['db']['dsn'], $config['db']['username'], $config['db']['password'],
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `utf8`',
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]
    );

    $filteredData = [];
    foreach ($required as $value => $filter) {
        $filteredData[$value] = filter_var($_POST[$value], $filter);
    }
    $pdo->exec("UPDATE `{$filteredData['table']}` SET `{$filteredData['field']}` = '{$filteredData['value']}' WHERE `ID` = {$filteredData['id']}");
    echo 'OK';
} catch (\PDOException $e) {
    http_response_code(500);
    echo $e->getMessage();
}