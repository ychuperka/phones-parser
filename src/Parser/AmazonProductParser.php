<?php

namespace Ychuperka\PhonesParser\Parser;

use Sergeylukin\AmazonProductSearch;
use Onoi\HttpRequest\HttpRequestFactory;

class AmazonProductParser extends AmazonProductSearch
{
    public function getLastFoundProductAsin()
    {
        if (!isset($this->xpath)) {
            return false;
        }

        $node = $this->xpath->query('//*[@id=\'averageCustomerReviews\']')->item(0);
        if (!$node) {
            return false;
        }

        return $node->nodeValue;
    }

    public function getLastFoundProductRating()
    {
        if (!isset($this->xpath)) {
            return false;
        }

        $asin = $this->getLastFoundProductAsin();

        $factory = new HttpRequestFactory();
        $request = $factory->newCurlRequest(
            'http://www.amazon.com/gp/customer-reviews/widgets/average-customer-review/popover/ref=dpx_acr_pop_'
                . '?contextId=dpx&asin=' . $asin
        );
        $response = $request->execute();
        if ($request->getLastErrorCode() !== CURLE_OK) {
            return false;
        }

        return $response;
    }

}