<?php

namespace Ychuperka\PhonesParser\Parser;

use Onoi\HttpRequest\CurlRequest;
use Onoi\HttpRequest\HttpRequestFactory;
use Sunra\PhpSimple\HtmlDomParser;
use Sergeylukin\AmazonProductSearch;

class Parser
{
    const FLUSH_COOKIES_EACH_PAGE = 20;

    private $enterUrl;
    private $groupsMap;
    private $attributesMap;
    private $cookiesFilename;
    private $curlHttpRequest;
    private $domain;
    private $minDelay;
    private $maxDelay;
    private $exceptionDelay;
    private $userAgents;
    private $currentUserAgent;
    private $isItemExistsCallback;
    private $badUrlSignatures;

    /**
     * @param string $enterUrl
     * @param array $groupsMap
     * @param array $attributesMap
     * @param string $cookiesFilename
     * @param int $minDelay
     * @param int $maxDelay
     * @param int $exceptionDelay
     * @throws Exception
     */
    public function __construct($enterUrl, $groupsMap, $attributesMap, $cookiesFilename, $minDelay = 1, $maxDelay = 5, $exceptionDelay = 120)
    {
        /*
         * Check enter url and extract a domain
         */
        if (!$enterUrl) {
            throw new Exception('Enter url is empty');
        }
        $urlParsed = @parse_url($enterUrl);
        if (!$urlParsed) {
            throw new Exception('Can not parse an enter url');
        }
        $this->enterUrl = $enterUrl;
        $this->domain = 'http://' . $urlParsed['host'];
        unset($urlParsed);

        /*
         * Check groups map
         */
        if (!$groupsMap) {
            throw new Exception('Groups map is empty');
        }
        $this->groupsMap = $groupsMap;

        /*
         * Check attributes map
         */
        foreach ($groupsMap as $group) {
            if (!isset($attributesMap[$group])) {
                throw new Exception('An item with key "' . $group . '" not found in attributes map"');
            }
            if (count($attributesMap[$group]) == 0) {
                throw new Exception('An item with key "' . $group . '" found in attributes map but it is empty');
            }
        }
        $this->attributesMap = $attributesMap;

        /*
        Check cookies file is writable, create it if not exists
        */
        if (!file_exists($cookiesFilename)) {
            if (!touch($cookiesFilename)) {
                throw new Exception("File \"$cookiesFilename\" is not writable");
            }
        } else {
            if (!is_writable($cookiesFilename)) {
                throw new Exception("File \"$cookiesFilename\" is not writable");
            }
        }
        $this->cookiesFilename = $cookiesFilename;

        /*
        Prepare a http request instance
        */
        $reqFactory = new HttpRequestFactory();
        $this->curlHttpRequest = $reqFactory->newCurlRequest();
        $this->setDefaultCurlOptions($this->curlHttpRequest);

        // Set delay values
        $this->minDelay = $minDelay;
        $this->maxDelay = $maxDelay;
        $this->exceptionDelay = $exceptionDelay;
    }

    /**
     * @param $callback
     * @throws Exception
     * @return $this
     */
    public function setIsItemExistsCallback($callback)
    {
        if (!is_callable($callback)) {
            throw new Exception('The callback is not a callable instance');
        }

        $this->isItemExistsCallback = $callback;

        return $this;
    }

    /**
     * @param array $items
     * @return $this
     */
    public function setBadUrlSignatures(array $items)
    {
        $this->badUrlSignatures = $items;
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function addUserAgent($value)
    {
        $this->userAgents[] = $value;
        return $this;
    }

    /**
     * @param $callback
     * @throws Exception
     */
    public function loadItems($callback)
    {
        if (!is_callable($callback)) {
            throw new Exception('Callback is not callable');
        }

        /*
         * Get page numbers
         */
        $pageNumbers = $this->getPageNumbers();

        /*
         * Go through page numbers
         */
        while (count($pageNumbers) > 0) {
            try {
                $this->flushCookies();
                $this->arrayRandomRefill($pageNumbers);
                echo 'Need to process ' . count($pageNumbers) . ' pages...' . PHP_EOL;
                $this->goThroughPages($pageNumbers, $callback);
            } catch (Exception $e) {
                echo 'WARNING! An exception occur: ' . $e->getMessage() . PHP_EOL . 'Now I will wait... and continue after '
                    . $this->exceptionDelay . ' seconds.' . PHP_EOL;
                sleep($this->exceptionDelay);
            }
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function getPageNumbers()
    {
        $lastPageNum = 0;
        for (; ;) {
            try {
                $pageContent = $this->loadListPage(0);
                $pageDom = $this->loadDOM($pageContent);
                $lastPageNum = $this->getLastPageNum($pageDom);
                break;
            } catch (Exception $e) {
                echo 'An exception occur: ' . $e->getMessage() . PHP_EOL . 'Waiting ' . $this->exceptionDelay
                    . ' seconds...' . PHP_EOL;
                sleep($this->exceptionDelay);
            }
        }

        $pageNumbers = [];
        for ($i = 1; $i <= $lastPageNum; ++$i) {
            $pageNumbers[] = $i;
        }

        return $pageNumbers;
    }

    /**
     * @param array $pageNumbers
     * @param $callback
     * @throws Exception
     */
    protected function goThroughPages(array &$pageNumbers, $callback)
    {

        $pagesCount = 0;

        foreach ($pageNumbers as $key => $pageNum) {

            // Load page and get links to items
            $pageContent = $this->loadListPage($pageNum);
            $pageDom = $this->loadDOM($pageContent);
            $links = $this->getItemsLinks($pageDom);
            if (count($links) == 0) {
                throw new Exception('Empty links list!', 0, null, $pageContent);
            }

            // Go through links
            foreach ($links as $l) {

                // If link has a bad url signature then continue the main loop
                foreach ($this->badUrlSignatures as $bs) {
                    if (strpos($l, $bs) !== false) {
                        echo 'Found bad url signature "' . $bs . '" in url "' . $l . '"' . PHP_EOL;
                        continue 2;
                    }
                }

                // Call callback to check if item already exists
                if ($this->isItemExistsCallback && call_user_func($this->isItemExistsCallback, $this->extractItemIdFromUrl($l))) {
                    echo 'Item already exists: ' . $l . PHP_EOL;
                    continue;
                }

                // Load item data
                echo 'Loading item from: ' . $l . PHP_EOL;
                $item = $this->loadItem($l);
                if (!$item) {
                    echo 'Can not load item from: ' . $l . PHP_EOL;
                    continue;
                }
                call_user_func($callback, $item);

                // If processed pages count equals limit, then flush cookies
                $pagesCount++;
                if ($pagesCount == self::FLUSH_COOKIES_EACH_PAGE) {
                    $pagesCount = 0;
                    $this->flushCookies();
                }

            }

            // Remove current page number from numbers list, we do not want to process the page again.
            unset($pageNumbers[$key]);

        }
    }

    /**
     * Flush cookies
     */
    protected function flushCookies()
    {
        file_put_contents($this->cookiesFilename, null);
        if ($this->userAgents) {
            $this->currentUserAgent = $this->userAgents[mt_rand(0, count($this->userAgents) - 1)];
        }
    }


    /**
     * @param int $pageNum
     * @return string
     * @throws Exception
     */
    protected function loadListPage($pageNum = 0)
    {
        sleep(
            mt_rand($this->minDelay, $this->maxDelay)
        );

        $pageNumAsString = $pageNum > 0 ? '?p=' . $pageNum : '';
        $url = $this->enterUrl . $pageNumAsString;
        $this->setCurlOptions($this->curlHttpRequest, [CURLOPT_URL => $url]);
        echo 'Loading page: ' . $url . PHP_EOL;
        $response = $this->curlHttpRequest->execute();
        $this->checkCurlError($this->curlHttpRequest);

        return $response;
    }

    /**
     * Get a last page number
     *
     * @param \simple_html_dom $dom
     * @return int
     * @throws Exception
     */
    protected function getLastPageNum(\simple_html_dom $dom)
    {
        $anchors = $dom->find('div.pager a[href^="?p="]');
        if (count($anchors) == 0) {
            throw new Exception('Can not find an last page number', 0, null, (string)$dom);
        }

        $max = 0;
        foreach ($anchors as $a) {
            $num = (int)preg_replace('/\D/', null, $a->href);
            if ($num == 0) {
                throw new Exception("Can not process anchor, href = {$a->href}", 0, null, (string)$dom);
            }
            if ($num > $max) {
                $max = $num;
            }
        }

        echo 'Last page number: ' . $max . PHP_EOL;

        return $max;
    }

    /**
     * @param \simple_html_dom $dom
     * @return array
     */
    protected function getItemsLinks(\simple_html_dom $dom)
    {
        $anchors = $dom->find('ul.catalog div.info div.ttle a');
        if (count($anchors) == 0) {
            return [];
        }

        $links = [];
        foreach ($anchors as $a) {
            $link = $a->href;
            if (strpos($link, '/prices') !== false) {
                $link = str_replace('/prices', null, $link);
            }
            if ($link{0} == '/') {
                $link = $this->domain . $link;
            }
            foreach ($this->badUrlSignatures as $bs) {
                if (strpos($link, $bs) !== false) {
                    echo 'Found bad url signature "' . $bs . '" in url "' . $link . '"' . PHP_EOL;
                    continue 2;
                }
            }
            $links[] = $link;

        }

        $this->arrayRandomRefill($links);

        return $links;
    }

    /**
     * @param $array
     */
    private function arrayRandomRefill(&$array)
    {
        usort(
            $array,
            function ($a, $b) {
                return mt_rand(-1, 1);
            }
        );
    }

    /**
     * @param $url
     * @return array
     * @throws Exception
     */
    protected function loadItem($url)
    {
        sleep(
            mt_rand($this->minDelay, $this->maxDelay)
        );

        $this->setCurlOptions(
            $this->curlHttpRequest,
            [
                CURLOPT_URL => $url,
            ]
        );
        $response = $this->curlHttpRequest->execute();
        $this->checkCurlError($this->curlHttpRequest);

        $dom = $this->loadDOM($response);

        $data = [
            'attributes' => $this->getItemAttributes($dom),
            'images' => $this->getItemImages($dom),
        ];
        if (count($data['attributes']) == 0) {
            return null;
        }

        $data['attributes']['ItemID'] = $this->extractItemIdFromUrl($url);

        return $data;
    }

    /**
     * @param string $url
     * @return mixed|string
     */
    protected function extractItemIdFromUrl($url)
    {
        $len = strlen($url);
        if ($url{$len - 1} == '/') {
            $url = substr($url, 0, $len - 1);
        }

        $lastSlashPos = strrpos($url, '/');
        if ($lastSlashPos === false) {
            return sha1($url);
        }

        return substr($url, $lastSlashPos + 1);
    }

    /**
     * @param \simple_html_dom $dom
     * @return array
     */
    protected function getItemImages(\simple_html_dom $dom)
    {
        $images = [];
        foreach ($dom->find('div#gallery-box a') as $anchor) {
            $images[] = $this->domain . $anchor->href;
        }
        return $images;
    }

    /**
     * @param \simple_html_dom $dom
     * @return array
     * @throws Exception
     */
    protected function getItemAttributes(\simple_html_dom $dom)
    {
        $groupsMap = $this->getGroupsMap();
        $attributesMap = $this->getAttributesMap();
        $currentGroup = 'main';
        $attributes = [];
        foreach ($dom->find('table#full-props-list tr') as $row) {

            $th = $row->find('th', 0);
            if ($th->class && $th->class == 'title') {
                $groupName = $this->convertEncoding(
                    trim(
                        $th->plaintext
                    )
                );
                if (!isset($groupsMap[$groupName])) {
                    throw new Exception("Can not find group map record for group \"$groupName\"");
                }
                $currentGroup = $groupsMap[$groupName];
                continue;
            }

            $attrName = $this->convertEncoding(
                trim(
                    $th->plaintext
                )
            );

            $setAttrValue = function () use ($row) {
                return $this->convertEncoding(
                    trim(
                        $row->find('td', 0)->plaintext
                    )
                );
            };
            if ($attrName == 'Товар на сайте производителя') {
                $possibleAnchor = $row->find('td a', 0);
                if ($possibleAnchor) {
                    $attrValue = $possibleAnchor->href;
                } else {
                    $attrValue = $setAttrValue();
                }
            } else {
                $attrValue = $setAttrValue();
            }

            $list = $attributesMap[$currentGroup];
            foreach ($list as $key => $value) {
                if ($key == $attrName) {
                    if (!isset($attributes[$value])) {
                        $attributes[$value] = '';
                    }
                    $attributes[$value] .= $attrValue . ' ';
                }
            }
        }

        foreach ($attributes as $key => $value) {
            $attributes[$key] = trim($value);
        }

        if (!isset($attributes['Brand'])) {
            $attributes['Brand'] = 'Unknown';
        }

        if (!isset($attributes['Model'])) {
            $itemNameHeader = $dom->find('h1[itemprop="name"]', 0);
            if ($itemNameHeader) {
                $attributes['Model'] = trim(
                    preg_replace(
                        '/[а-яА-Я]+/u', null,
                        preg_replace('/\(.+\)/', null, $itemNameHeader->plaintext)
                    )
                );
            }
        }

        $pubDateSpan = $dom->find('div.th-info div.th span.more', 0);
        if ($pubDateSpan) {
            $pubDate = \DateTime::createFromFormat(
                'dmY',
                preg_replace('/[\D]/', null, $pubDateSpan->plaintext)
            );
            if (!$pubDate) {
                throw new Exception('Can not convert span contents to DateTime instance');
            }

            $attributes['ReleaseDate'] = $pubDate->format('Y-m-d');
        }

        return $attributes;
    }

    /**
     * @param $value
     * @return string
     */
    protected function convertEncoding($value)
    {
        return mb_convert_encoding(
            $value,
            'utf-8',
            mb_detect_encoding($value)
        );
    }

    /**
     * @param CurlRequest $req
     * @throws Exception
     */
    protected function checkCurlError(CurlRequest $req)
    {
        if ($req->getLastErrorCode() != CURLE_OK) {
            throw new Exception("Curl error occur: {$req->getLastError()}");
        }
    }

    /**
     * @param CurlRequest $req
     */
    protected function setDefaultCurlOptions(CurlRequest $req)
    {
        $options = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_COOKIEFILE => $this->cookiesFilename,
            CURLOPT_COOKIEJAR => $this->cookiesFilename,
        ];
        if ($this->currentUserAgent) {
            $options[CURLOPT_USERAGENT] = $this->currentUserAgent;
        }
        $this->setCurlOptions($req, $options);
    }

    /**
     * @param $content
     * @return mixed
     * @throws Exception
     */
    protected function loadDOM($content)
    {
        $dom = HtmlDomParser::str_get_html($content);
        if ($dom === false) {
            throw new Exception('Can not load DOM');
        }

        return $dom;
    }

    /**
     * @param CurlRequest $req
     * @param array $options
     */
    protected function setCurlOptions(CurlRequest $req, array $options)
    {
        foreach ($options as $key => $value) {
            $req->setOption($key, $value);
        }
    }


    /**
     * @return array
     */
    protected function getGroupsMap()
    {
        return $this->groupsMap;
    }

    /**
     * @return array
     */
    protected function getAttributesMap()
    {
        return $this->attributesMap;
    }
}