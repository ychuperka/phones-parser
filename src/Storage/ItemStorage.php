<?php

namespace Ychuperka\PhonesParser\Storage;


use Ychuperka\PhonesParser\Storage\DbStorage;
use Ychuperka\PhonesParser\Storage\Exception as StorageException;

/**
 * Class ItemStorage
 * @package Ychuperka\PhonesParser\Storage\Item
 */
class ItemStorage extends DbStorage
{

    /**
     * @param array $db
     * @param string $table
     * @throws \Ychuperka\PhonesParser\Storage\Exception
     */
    public function __construct(array $db, $table)
    {
        parent::__construct($db, $table, 'ItemID');
    }

    /**
     * Saves an item and returns last inserted row id
     *
     * @param array $attributes
     * @return string
     * @throws StorageException
     */
    public function save(array $attributes)
    {
        if (count($attributes) == 0) {
            throw new StorageException('Attributes assoc should be non-empty');
        }

        /*
         * Prepare fields.
         * Put each attribute key into list
         */
        $fields = array_keys($attributes);
        $fieldsPrepared = [];
        foreach ($fields as $fl) {
            if (!$this->isFieldExists($fl)) {
                throw new StorageException("Field \"$fl\" is not found in table \"{$this->getTable()}\"");
            }
            $fieldsPrepared[] = "`$fl`";
        }

        /*
         * Prepare query
         */
        $sql = 'INSERT INTO `' . $this->getTable() . '` (' . implode(', ', $fieldsPrepared) . ') VALUES (';
        unset($fieldsPrepared);

        /*
         * Prepare values section
         */
        $valuesPrepared = [];
        foreach ($attributes as $attr) {
            $valuesPrepared[] = $this->getPdo()->quote($attr);
        }
        $sql .= implode(', ', $valuesPrepared) . ')';


        try {
            $this->getPdo()->exec($sql);
        } catch (\PDOException $e) {
            $this->getPdo(true)->exec($sql);
        }

        return $this->getPdo()->lastInsertId();
    }

}