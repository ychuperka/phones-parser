<?php

namespace Ychuperka\PhonesParser\Storage;

use Ychuperka\PhonesParser\Storage\IImageStorage;
use Ychuperka\PhonesParser\Storage\Exception as StorageException;

/**
 * Class ImageStorage
 * @package Ychuperka\PhonesParser\Storage\Item
 */
class ImageStorage implements IImageStorage
{
    private $directoryIter;

    /**
     * @var string
     */
    private $directory;

    /**
     * @param string $directory
     * @throws StorageException
     */
    public function __construct($directory)
    {

        /*
         * Check exif extension
         */
        if (!extension_loaded('gd')) {
            throw new StorageException('An extension "gd" is required');
        }

        /*
         * Check directory exists
         */
        if (!is_dir($directory)) {
            throw new StorageException("The path \"$directory\" is not a directory path");
        } else if (!is_writable($directory)) {
            throw new StorageException("The directory \"$directory\" is not writable");
        }
        $this->directory = $directory;
    }

    /**
     * @param string $name
     * @param string $contents
     * @throws StorageException
     */
    public function saveImage($name, $contents)
    {
        /*
         * Check params
         */
        if (!$name || !$contents) {
            throw new StorageException('Name or contents are empty');
        }

        /*
         * Try to determine image extension
         */
        $imageInfo = getimagesizefromstring($contents);
        if (!$imageInfo) {
            throw new StorageException('Can not read image info');
        }
        $extension = image_type_to_extension($imageInfo[2], true);
        if ($extension === '.jpeg') {
            $extension = '.jpg';
        }

        /*
         * Save image
         */
        $filename = $name . $extension;
        file_put_contents($this->directory . DIRECTORY_SEPARATOR . $filename, $contents);
    }

    /**
     * @param string $name
     * @return boolean
     */
    public function deleteImage($name)
    {
        $result = false;
        $this->search(
            $name,
            function (\DirectoryIterator $item) use (&$result) {
                $result = true;
                unlink($item->getRealPath());
            }
        );
        return $result;
    }

    /**
     * @param string $name
     * @return bool
     * @throws StorageException
     */
    public function imageExists($name)
    {
        $result = false;
        $this->search(
            $name,
            function (\DirectoryIterator $item) use (&$result) {
                $result = true;
            }
        );
        return $result;
    }

    /**
     * @param string $name
     * @param mixed $callback
     * @throws StorageException
     */
    protected function search($name, $callback)
    {
        if (!is_callable($callback)) {
            throw new StorageException('The "callback" is not a callable instance');
        }

        foreach ($this->getDirectoryIterator() as $item) {
            if ($item->getBasename('.' . $item->getExtension()) != $name) {
                continue;
            }

            call_user_func($callback, $item);
            break;
        }
    }

    /**
     * @return \DirectoryIterator
     */
    protected function getDirectoryIterator()
    {
        if ($this->directoryIter) {
            return $this->directoryIter;
        }

        $this->directoryIter = new \DirectoryIterator($this->directory);
        return $this->directoryIter;
    }
}