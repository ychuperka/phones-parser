<?php

namespace Ychuperka\PhonesParser\Storage;

interface IImageStorage
{
    public function saveImage($name, $contents);
    public function deleteImage($name);
    public function imageExists($name);
}