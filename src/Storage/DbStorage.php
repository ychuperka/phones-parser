<?php
namespace Ychuperka\PhonesParser\Storage;

/**
 * Class DbStorage
 * @package Ychuperka\PhonesParser\Storage
 */
abstract class DbStorage implements IStorage
{
    const CONNECTION_ALIVE_TIMEOUT = 10;

    /**
     * @var int
     */
    private $lastPdoAccessTimestamp;

    /**
     * @var array
     */
    private $dbConnectionData;

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var string
     */
    private $table;

    /**
     * @var array
     */
    private $tableData;

    /**
     * @var string
     */
    private $idField;

    /**
     * @param array $db
     * @param string $table
     * @param string $idField
     * @throws Exception
     */
    public function __construct(array $db, $table, $idField)
    {
        /*
         * Store db connection data
         */
        $required = ['dsn', 'username', 'password'];
        foreach ($required as $field) {
            if (!isset($db[$field])) {
                throw new Exception('Required option "' . $field . '" is not found"');
            }
        }
        $this->dbConnectionData = $db;

        /*
         * Try to connect to the database
         */
        $this->getPdo();

        /*
         * Get table data
         */
        $this->table = filter_var($table, FILTER_SANITIZE_STRING);
        $this->tableData = $this->pdo->query('SHOW CREATE TABLE `' . $this->table . '`', \PDO::FETCH_ASSOC)->fetch();

        /*
         * Set id field
         */
        // TODO: Check field exists
        $this->idField = $idField;
    }

    /**
     * @param bool $new
     * @return \PDO
     */
    public function getPdo($new = false)
    {
        if (!$new && $this->pdo && $this->lastPdoAccessTimestamp > time() - self::CONNECTION_ALIVE_TIMEOUT) {
            $this->lastPdoAccessTimestamp = time();
            return $this->pdo;
        }

        $this->pdo = new \PDO(
            $this->dbConnectionData['dsn'], $this->dbConnectionData['username'], $this->dbConnectionData['password'],
            [
                \PDO::ATTR_PERSISTENT => true,
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `utf8`',
            ]
        );

        $this->lastPdoAccessTimestamp = time();

        return $this->pdo;
    }

    /**
     * @return array|mixed
     */
    public function getTableData()
    {
        return $this->tableData;
    }

    /**
     * @return mixed|string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getIdField()
    {
        return $this->idField;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function getList($offset, $limit)
    {
        if (!is_numeric($offset) || !is_numeric($limit)) {
            throw new Exception('Offset or limit has not numeric values');
        }
        $statement = $this->getPdo()->prepare('SELECT * FROM `' . $this->getTable() . '` LIMIT ' . $offset . ',' . $limit);
        $statement->execute();

        if ($statement->rowCount() == 0) {
            return [];
        }

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function get($id)
    {
        $statement = $this->getPdo()->prepare('SELECT * FROM `' . $this->getTable() . '` WHERE `' . $this->getIdField() . '` = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        return $statement->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists($id)
    {
        $statement = $this->getPdo()->prepare('SELECT COUNT(*) FROM `' . $this->getTable() . '` WHERE `' . $this->getIdField() . '` = :id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        return $statement->fetchColumn(0) > 0;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function delete($id)
    {
        $statement = $this->getPdo()->prepare('DELETE FROM `' . $this->getTable() . '` WHERE `' . $this->getIdField(). '` = :id');
        $statement->bindValue(':id', $id);
        return $statement->execute();
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function isFieldExists($name)
    {
        // TODO: Implement
        return true;
    }
}