<?php

namespace Ychuperka\PhonesParser\Storage;

interface IStorage
{
    public function save(array $attributes);
    public function delete($id);
    public function get($id);
    public function getList($offset, $limit);
    public function exists($id);
}