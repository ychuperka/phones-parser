<?php

namespace Ychuperka\PhonesParser\Proxy;

/**
 * Class Proxy
 *
 * @package Ychuperka\PhonesParser\Proxy
 */
class Proxy
{
    /**
     * @var boolean
     */
    private $locked;

    /**
     * Lock the proxy
     */
    public function lock()
    {
        $this->locked = true;
    }

    /**
     * @return mixed
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * Unlock the proxy
     */
    public function unlock()
    {
        $this->locked = false;
    }

    /**
     * @param string $name
     * @return mixed
     * @throws ProxyException
     */
    public function __get($name)
    {
        if (!isset($this->$name)) {
            throw new ProxyException("The property \"$name\" not found");
        }
        return $this->$name;
    }
}