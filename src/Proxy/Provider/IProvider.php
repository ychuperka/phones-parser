<?php

namespace Ychuperka\PhonesParser\Proxy\Provider;

interface IProvider
{
    public function getProxies();
}