<?php

namespace Ychuperka\PhonesParser\Proxy\Provider;

use Onoi\HttpRequest\HttpRequestFactory;
use Ychuperka\PhonesParser\Proxy\Proxy;
use Ychuperka\PhonesParser\Proxy\ProxyException;

/**
 * Class HidemeApi
 *
 * @package Ychuperka\PhonesParser\Proxy\Provider
 */
class HidemeApi implements IProvider
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var array
     */
    private $params;

    /**
     * @param string $code
     * @param array $params
     */
    public function __construct($code, array $params = [])
    {
        $this->code = $code;
        $this->params = $params;
    }

    /**
     * @return array|bool
     * @throws ProxyException
     */
    public function getProxies()
    {
        $params = $this->params;
        $params['code'] = $this->code;
        $params['out'] = 'php';
        $request = (new HttpRequestFactory())->newCurlRequest(
            'http://hideme.ru/api/proxylist.php?' . http_build_query($params)
        );
        $request->setOption(CURLOPT_RETURNTRANSFER, true);
        $request->setOption(CURLOPT_FOLLOWLOCATION, true);
        $response = $request->execute();
        if ($request->getLastErrorCode() != CURLE_OK) {
            throw new ProxyException('Curl error occur, message: ' . $request->getLastError() . ', code: ' . $request->getLastErrorCode()
                . PHP_EOL);
        }

        $list = unserialize($response);
        $proxies = [];
        foreach ($list as $item) {
            $proxy = new Proxy();
            foreach ($item as $k => $v) {
                $proxy->$k = $v;
            }
            $proxies[] = $proxy;
        }

        return $proxies;
    }

}