<?php

namespace Ychuperka\PhonesParser\Proxy\Provider;

use Ychuperka\PhonesParser\Proxy\Proxy;
use Ychuperka\PhonesParser\Proxy\ProxyException;

/**
 * Class Csv
 *
 * @package Ychuperka\PhonesParser\Proxy\Provider
 */
class Csv implements IProvider
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var int
     */
    private $maxDelay;

    /**
     * @var int
     */
    private $minAnon;

    /**
     * @var array
     */
    private $ignoreCountryCodes;

    /**
     * @param string $path
     * @param int $maxDelay
     * @param int $minAnon
     * @param array $ignoreCountryCodes
     * @throws ProxyException
     */
    public function __construct($path, $maxDelay = 2000, $minAnon = 3, array $ignoreCountryCodes = [])
    {
        if (!file_exists($path)) {
            throw new ProxyException("The file \"$path\" not found");
        }
        $this->path = $path;
        $this->maxDelay = $maxDelay;
        $this->minAnon = $minAnon;
        $this->ignoreCountryCodes = $ignoreCountryCodes;
    }


    /**
     * @return array
     * @throws ProxyException
     * @throws \Exception
     */
    public function getProxies()
    {
        $proxies = [];
        $fHnd = fopen($this->path, 'r');
        $rowIndex = 0;
        $map = [];
        while (!feof($fHnd)) {

            $row = fgetcsv($fHnd, null, ';');
            if ($rowIndex == 0) {
                $colsCount = count($row);
                for ($i = 0; $i < $colsCount; ++$i) {
                    $map[$i] = $row[$i];
                }
                $rowIndex++;
                continue;
            }

            $proxy = new Proxy();
            $rowItemsCount = count($row);
            for ($i = 0; $i < $rowItemsCount; ++$i) {
                $propertyName = $map[$i];
                $proxy->$propertyName = $row[$i];
            }

            if ($proxy->delay > $this->maxDelay
                || $proxy->anon < $this->minAnon
                || in_array($proxy->country_code, $this->ignoreCountryCodes)) {
                continue;
            }

            $proxies[] = $proxy;
        }
        fclose($fHnd);

        return $proxies;
    }
}