<?php

namespace Ychuperka\PhonesParser\Proxy;

use Ychuperka\PhonesParser\Proxy\Provider\IProvider;

/**
 * Class Manager
 *
 * @package Ychuperka\PhonesParser\Proxy
 */
class Manager
{
    /**
     * @var array
     */
    private $proxies;
    private $provider;
    private $nextIndex;
    private $count;

    /**
     * @param IProvider $provider
     */
    public function __construct(IProvider $provider)
    {
        $this->provider = $provider;
        $this->proxies = $provider->getProxies();
        usort(
            $this->proxies,
            function ($a, $b) {
                return mt_rand(0, 1);
            }
        );
        $this->count = count($this->proxies);
        $this->nextIndex = 0;
    }

    /**
     * Get proxy
     *
     * @return Proxy|bool
     * @throws ProxyException
     */
    public function getProxy()
    {
        if (!$this->proxies) {
            $this->proxies = $this->provider->getProxies();
        }
        for (; ;) {
            $idx = $this->nextIndex;
            $this->nextIndex++;
            if ($this->nextIndex == $this->count) {
                $this->nextIndex = 0;
            }
            $proxy = $this->proxies[$idx];
            if ($proxy->isLocked()) {
                continue;
            }
            $proxy->lock();
            return $proxy;
        }
    }

    /**
     * Release proxies
     *
     * @param array $proxies
     */
    public function release(array $proxies)
    {
        foreach ($proxies as $p) {
            $p->unlock();
        }
    }

    /**
     * Remove proxy from a list
     *
     * @param Proxy $proxy
     */
    public function remove(Proxy $proxy)
    {
        $count = count($this->proxies);
        for ($i = 0; $i < $count; $i++) {
            if ($proxy !== $this->proxies[$i]) {
                continue;
            }
            unset($this->proxies[$i]);
            usort(
                $this->proxies,
                function ($a, $b) {
                    return mt_rand(0, 1);
                }
            );
            $this->nextIndex = 0;
            break;
        }
    }
}