<?php

namespace Ychuperka\PhonesParser\Translator\Provider;

interface IProvider
{
    public function translate($value);
}