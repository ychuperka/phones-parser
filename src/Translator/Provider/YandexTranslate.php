<?php

namespace Ychuperka\PhonesParser\Translator\Provider;

use Onoi\Cache\Cache;
use Ychuperka\PhonesParser\Translator\Exception as TranslatorException;

/**
 * Class YandexTranslate
 * @package Ychuperka\PhonesParser\Translator\Provider
 */
class YandexTranslate extends AbstractProvider
{
    use JSONResponseProviderTrait;

    const API_METHOD_URL_GET_LANGS = 'https://translate.yandex.net/api/v1.5/tr.json/getLangs';
    const API_METHOD_URL_TRANSLATE = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var array
     */
    private $availableDirections;

    /**
     * @param string $apiKey
     * @param Cache $cache
     * @throws TranslatorException
     */
    public function __construct($apiKey, Cache $cache = null)
    {
        parent::__construct($cache);

        $this->apiKey = $apiKey;
        $this->cache = $cache;
    }

    /**
     * @param string $value
     * @param string $from
     * @param string $to
     * @return string
     */
    public function translate($value, $from = 'ru', $to = 'en')
    {
        if (strlen($value) == 0) {
            return '';
        }

        return $this->doTranslation($value, $from, $to);
    }

    /**
     * @param string $value
     * @param string $from
     * @param string $to
     * @return mixed
     * @throws TranslatorException
     */
    protected function doTranslation($value, $from, $to)
    {
        $direction = "$from-$to";
        if (!$this->isDirectionSupported($direction)) {
            throw new TranslatorException("The direction \"$direction\" is not supported");
        }

        $url = self::API_METHOD_URL_TRANSLATE . '?'
            . http_build_query(
                [
                    'key' => $this->apiKey,
                    'text' => $value,
                    'lang' => $direction,
                    'format' => 'plain',
                ]
            );
        $request = $this->prepareCurlRequest($url);
        $response = $this->decodeResponse(
            $this->makeRequest($request)
        );
        if (count($response) == 0 || empty($response['text'])) {
            throw new TranslatorException('Empty translation result');
        }

        return $response['text'][0];
    }

    /**
     * Direction is supported?
     *
     * Direction examples: "ru-en", "en-ru", "en-ar", ...
     *
     * @param string $direction
     * @return bool
     * @throws TranslatorException
     */
    public function isDirectionSupported($direction)
    {
        return in_array($direction, $this->getDirections());
    }

    /**
     * @return mixed
     * @throws TranslatorException
     */
    public function getDirections()
    {
        if ($this->availableDirections) {
            return $this->availableDirections;
        }

        $url = self::API_METHOD_URL_GET_LANGS . '?key=' . $this->apiKey;
        $request = $this->prepareCurlRequest($url);
        $response = $this->decodeResponse(
            $this->makeRequest($request)
        );
        if (count($response) == 0 || empty($response['dirs'])) {
            throw new TranslatorException('Can not get translation directions');
        }
        $this->availableDirections = $response['dirs'];
        return $this->availableDirections;
    }
}