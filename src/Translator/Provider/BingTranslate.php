<?php

namespace Ychuperka\PhonesParser\Translator\Provider;

use Ychuperka\PhonesParser\Cache\ICache;
use Ychuperka\PhonesParser\Translator\Exception as TranslatorException;

class BingTranslate extends AbstractProvider
{
    use JSONResponseProviderTrait;

    const API_METHOD_GET_ACCESS_TOKEN_GRANT_TYPE = 'client_credentials';
    const API_METHOD_GET_ACCESS_TOKEN_SCOPE_URL = 'http://api.microsofttranslator.com';
    const API_METHOD_GET_ACCESS_TOKEN_URL = 'https://datamarket.accesscontrol.windows.net/v2/OAuth2-13/';

    const API_METHOD_TRANSLATE = 'http://api.microsofttranslator.com/v2/Http.svc/Translate';

    private $clientId;
    private $clientSecret;
    private $accessToken;
    private $accessTokenExpiresIn;

    /**
     * @param null|\Onoi\Cache\Cache $clientId
     * @param $clientSecret
     * @param ICache|null $cache
     */
    public function __construct($clientId, $clientSecret, ICache $cache = null)
    {
        parent::__construct($cache);

        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }


    /**
     * @param string $value
     * @param string $from
     * @param string $to
     * @return mixed|string
     * @throws TranslatorException
     */
    public function translate($value, $from = 'ru', $to = 'en')
    {
        if (!$value) {
            return '';
        }

        $options = [
            CURLOPT_HTTPHEADER => [
                'Authorization: Bearer ' . $this->getToken(),
                'Content-Type: text/xml'
            ],
        ];
        $url = self::API_METHOD_TRANSLATE . '?' . http_build_query(
                [
                    'text' => $value,
                    'to' => $to,
                    'from' => $from,
                    'appId' => 'Bearer ' . $this->getToken(),
                ]
            );
        $request = $this->prepareCurlRequest($url, $options);
        $response = simplexml_load_string(
            $this->makeRequest($request)
        );

        if (!isset($response[0])) {
            throw new TranslatorException('Empty translation result');
        }

        return (string)$response[0];
    }

    /**
     * @return string
     * @throws TranslatorException
     */
    protected function getToken()
    {
        if ($this->accessToken || $this->accessTokenExpiresIn > time()) {
            return $this->accessToken;
        }

        $params = [
            'grant_type' => self::API_METHOD_GET_ACCESS_TOKEN_GRANT_TYPE,
            'scope' => self::API_METHOD_GET_ACCESS_TOKEN_SCOPE_URL,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
        ];
        $url = self::API_METHOD_GET_ACCESS_TOKEN_URL;
        $request = $this->prepareCurlRequest($url, [CURLOPT_POSTFIELDS => http_build_query($params)]);
        $response = $this->makeRequest($request);

        $decoded = $this->decodeResponse($response);
        $requiredFields = [
            'access_token', 'expires_in'
        ];
        foreach ($requiredFields as $field) {
            if (empty($decoded[$field])) {
                throw new TranslatorException('Required field "' . $field . '" is not found in response');
            }
        }

        if (!is_numeric($decoded['expires_in'])) {
            throw new TranslatorException('The value of "expires_in" is not numeric');
        }

        $this->accessToken = $decoded['access_token'];
        $this->accessTokenExpiresIn = time() + (int)$decoded['expires_in'];
        return $this->accessToken;
    }
}