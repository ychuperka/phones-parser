<?php

namespace Ychuperka\PhonesParser\Translator\Provider;

use Onoi\HttpRequest\CurlRequest;
use Onoi\HttpRequest\HttpRequestFactory;
use Onoi\Cache\Cache;
use Ychuperka\PhonesParser\Translator\Exception as TranslatorException;

abstract class AbstractProvider implements IProvider
{

    /**
     * @var HttpRequestFactory
     */
    private $requestFactory;

    /**
     * @param Cache|null $cache
     */
    public function __construct(Cache $cache = null)
    {
        $this->requestFactory = new HttpRequestFactory($cache);
    }

    /**
     * @param string $url
     * @param array $options
     * @return \Onoi\HttpRequest\CachedCurlRequest
     */
    protected function prepareCurlRequest($url, array $options = null)
    {
        $curlRequest = $this->requestFactory->newCurlRequest($url);
        $defaultOptions = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 15,
        ];
        foreach ($defaultOptions as $key => $value) {
            $curlRequest->setOption($key, $value);
        }
        if (is_array($options)) {
            foreach ($options as $key => $value) {
                $curlRequest->setOption($key, $value);
            }
        }

        return $curlRequest;
    }

    /**
     * @param CurlRequest $request
     * @return mixed
     * @throws TranslatorException
     */
    protected function makeRequest(CurlRequest $request)
    {
        $response = null;
        for (; ;) {
            $response = $request->execute();
            switch ($request->getLastErrorCode()) {
                case CURLE_OK:
                    break 2;
                case CURLE_COULDNT_CONNECT:
                    echo 'Could`nt connect, retrying...' . PHP_EOL;
                    continue;
                    break;
                case CURLE_OPERATION_TIMEOUTED:
                    echo 'Operation timeout, retrying...' . PHP_EOL;
                    continue;
                    break;
                default:
                    throw new TranslatorException(
                        'Curl error #' . $request->getLastErrorCode() . ', message: '
                        . $request->getLastError()
                    );
            }
        }

        return $response;
    }
}