<?php

namespace Ychuperka\PhonesParser\Translator\Provider;

use Ychuperka\PhonesParser\Translator\Exception as TranslatorException;

trait JSONResponseProviderTrait
{
    private $extensionLoaded = false;

    /**
     * @param string $response
     * @return mixed
     * @throws TranslatorException
     */
    private function decodeResponse($response)
    {

        $this->extensionLoaded = extension_loaded('json');
        if (!$this->extensionLoaded) {
            throw new TranslatorException('An extension "json" is required');
        }

        $decoded = json_decode($response, true);
        if (!$decoded) {
            throw new TranslatorException('Can not decode server response');
        }

        if (isset($decoded['code']) && $decoded['code'] != 200) {
            throw new TranslatorException('Server error occur, code: ' . $decoded['code']);
        }

        return $decoded;
    }
}