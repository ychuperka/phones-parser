<?php

namespace Ychuperka\PhonesParser\Translator;

use Ychuperka\PhonesParser\Translator\Provider\IProvider;

/**
 * Class Translator
 * @package Ychuperka\PhonesParser\Translator
 */
class Translator
{
    /**
     * @var IProvider
     */
    private $provider;

    /**
     * @param IProvider $provider
     */
    public function __construct(IProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param string $value
     * @return string
     */
    public function translate($value)
    {
        return $this->provider->translate($value);
    }
}