<?php

namespace Ychuperka\PhonesParser\Cache;

use Ychuperka\PhonesParser\Cache\Exception as CacheException;

/**
 * Class FileCache
 * @package Ychuperka\PhonesParser\Cache
 */
class FileCache implements ICache
{
    const FLUSH_EACH_SET_OP = 10;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @var array
     */
    private $data;

    private $setOpsCount = 0;

    /**
     * @param string $filePath
     * @throws CacheException
     */
    public function __construct($filePath)
    {
        $fileExists = file_exists($filePath);
        if (!$fileExists && !touch($filePath)) {
            throw new CacheException("File \"$filePath\" not found and can not be created");
        }
        if (!is_writable($filePath)) {
            throw new CacheException("File \"$filePath\" is not writable");
        }
        $this->filePath = $filePath;

        if ($fileExists) {
            $this->data = @unserialize(
                file_get_contents($this->filePath)
            );
            if (!$this->data) {
                $this->data = [];
            }
        } else {
            $this->data = [];
        }
    }


    public function __destruct()
    {
        $this->flush();
    }

    public function flush()
    {
        file_put_contents(
            $this->filePath, serialize($this->data)
        );
    }

    public function get($key)
    {
        return $this->data[$key];
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;

        $this->setOpsCount++;
        if ($this->setOpsCount == self::FLUSH_EACH_SET_OP) {
            $this->flush();
            $this->setOpsCount = 0;
        }
    }

    public function delete($key)
    {
        unset($this->data[$key]);
    }

    public function exists($key)
    {
        return isset($this->data[$key]);
    }
}