<?php

namespace Ychuperka\PhonesParser\Cache;

interface ICache
{
    public function get($key);
    public function set($key, $value);
    public function delete($key);
    public function exists($key);
}