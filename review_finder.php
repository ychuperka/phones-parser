<?php

require_once __DIR__ . '/args.php';
require_once __DIR__ . '/vendor/autoload.php';

use Ychuperka\PhonesParser\Storage\ItemStorage;

$targetData = $config['targets'][TARGET];

$itemStorage = new ItemStorage(
    $config['db'], $targetData['table_translated']
);

$client = new Google_Client();
$client->setDeveloperKey($config['review_finder']['key']);
$client->setAuth(
    new Google_Auth_Simple($client)
);
$service = new Google_Service_YouTube($client);

$offset = 0;
$limit = $config['review_finder']['limit'];
for (; ;) {

    echo 'Selecting items, offset = ' . $offset, ', limit = ' . $limit . ' ...' . PHP_EOL;
    $list = $itemStorage->getList($offset, $limit);
    if (count($list) == 0) {
        break;
    }
    $offset += $limit;

    foreach ($list as $item) {

        if ($item['VideoReviewID']) {
            //  echo "Item \"{$item['ItemID']}\" already has a video review, continue...\n";
            continue;
        }

        echo 'Trying to find a review...' . PHP_EOL;
        $query = $item['Brand'] . ' ' . $item['Model'] . ' review in english';
        echo 'Query is "' . $query . '"...' . PHP_EOL;

        $result = $service->search->listSearch(
            'snippet',
            [
                'type' => 'video',
                'q' => $query,
                //'videoEmbeddable' => 'true',
                'relevanceLanguage' => $config['review_finder']['relevanceLanguage'],
                'regionCode' => $config['review_finder']['regionCode'],
                'maxResults' => 50,
            ]
        );
        if (count($result) == 0) {
            echo 'WARNING! Save the women and children! Empty search result.' . PHP_EOL;
            continue;
        }

        // Find item with latin characters only
        $validItem = null;
        $pattern = '/^[A-z[:print:]]{8,}$/u';
        $descPattern = '/^[A-z[:print:]]*$/u';
        $badSignatures = [
            ' for', 'case', 'cover',
            'stylus', 'housing', 'screen',
            'protector', 'tempered',
        ];
        foreach ($result as $videoItem) {
            if (preg_match($pattern, $videoItem->snippet->title)
                && preg_match($descPattern, $videoItem->snippet->description)
                && stripos($videoItem->snippet->title, 'review') !== false
                && call_user_func(function () use ($item, $videoItem) {
                    $parts = explode(' ', $item['Model']);
                    $partsCount = count($parts);
                    $count = 0;
                    foreach ($parts as $p) {
                        $p = str_replace(['(', ')'], null, $p);
                        if (stripos($videoItem->snippet->title, $p) !== false) {
                            $count++;
                        }
                        if ($count == $partsCount) {
                            return true;
                        }
                    }
                    return false;
                })
                && call_user_func(
                    function () use ($badSignatures, $videoItem) {
                        foreach ($badSignatures as $bs) {
                            if (stripos($videoItem->snippet->title, $bs) !== false) {
                                return false;
                            }
                        }
                        return true;
                    }
                )
            ) {
                $validItem = $videoItem;
                break;
            }
        }
        if ($validItem === null) {
            echo 'WARNING! Video for item "' . $item['Brand'] . ' ' . $item['Model'] . '" not found!"' . PHP_EOL;
            continue;
        }


        $videoId = $validItem->id->videoId;
        echo 'Review found, video id is "' . $videoId . '"...' . PHP_EOL;
        $itemStorage->getPdo()->exec("UPDATE `{$targetData['table_translated']}` SET `VideoReviewID` = '{$validItem->id->videoId}' WHERE `ID` = {$item['ID']}");
    }

}
echo 'Done.' . PHP_EOL;


