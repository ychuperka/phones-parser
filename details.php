<?php
header('Content-Type: text/html; charset=utf8');
$table = isset($_GET['table']) ? $_GET['table'] : 'phones_ru';
$table = filter_var($table, FILTER_SANITIZE_STRING);

$id = isset($_GET['id']) ? $_GET['id'] : null;
if ($id === null) {
    die ('Required parameter "id" is missing');
}
$id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

$params = require_once(__DIR__ . '/config.php');
$pdo = new PDO(
    $params['db']['dsn'], $params['db']['username'], $params['db']['password'],
    [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `utf8`',
    ]
);

/*
 * Check table exists
 */
try {
    $pdo->query('SELECT 1 FROM `' . $table . '` LIMIT 1');
} catch (\PDOException $e) {
    die('Sorry, a table "' . $table . '" is not found!');
}

$statement = $pdo->prepare(
    "SELECT * FROM `{$table}` WHERE `ID` = :id"
);
$statement->bindValue(':id', $id);
$statement->execute();
?>
    <a href="details.php?id=<?= $id - 1 ?>&table=<?= $table ?>">&larr; Previous</a>
    <a href="details.php?id=<?= $id + 1 ?>&table=<?= $table ?>">Next &rarr;</a>
<?php if ($statement->rowCount() == 0): ?>
    <h1>Item not found or not exists!</h1>
<?php else: ?>
    <?php $item = $statement->fetch(PDO::FETCH_ASSOC); ?>
    <ul>
        <?php foreach ($item as $key => $value): ?>
            <li><?= $key ?>: <?= $value ?></li>
        <?php endforeach; ?>
    </ul>
    <?php if (isset($item['VideoReviewID']) && $item['VideoReviewID']): ?>
        <iframe title="YouTube video player" class="youtube-player" type="text/html" width="640"
                height="390" src="http://www.youtube.com/embed/<?= $item['VideoReviewID'] ?>" frameborder="0"
                allowFullScreen></iframe>
    <?php endif; ?>
<?php endif; ?>