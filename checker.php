<?php
header('Content-Type: text/html; charset=utf8');
$tableName = isset($_REQUEST['table_name']) ? filter_var($_REQUEST['table_name'], FILTER_SANITIZE_STRING) : '';
$page = isset($_REQUEST['page']) ? filter_var($_REQUEST['page'], FILTER_SANITIZE_NUMBER_INT) : 0;


$totalCount = $pagesCount = $rowsCount = 0;
$rows = [];

if (strlen($tableName) > 0) {

    $config = require_once(__DIR__ . '/config.php');
    $target = null;
    foreach ($config['targets'] as $t) {
        if ($t['table'] !== $tableName && $t['table_translated'] !== $tableName) {
            continue;
        }
        $target = $t;
        break;
    }
    if ($target === null) {
        echo 'Target not found, table name "' . $tableName . '"' . PHP_EOL;
        exit(1);
    }

    /*
     * Connect to the database
     */
    $pdo = new PDO(
        $config['db']['dsn'], $config['db']['username'], $config['db']['password'],
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES `utf8`',
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]
    );

    /*
     * Check table exists
     */
    try {
        $pdo->query('SELECT 1 FROM `' . $tableName . '` LIMIT 1');
    } catch (\PDOException $e) {
        die('Sorry, a table "' . $tableName . '" is not found!');
    }

    /*
     * Select rows
     */
    $limit = $config['checker']['limit'];
    $offset = $limit * $page;
    $clauses = [];
    foreach ($target['checker_fields'] as $fl) {
        $clauses[] = ' CONVERT(`' . $fl . '` USING `cp1251`) COLLATE `cp1251_general_cs` RLIKE \'[а-я]+\'';
    }
    $sqlToSelectData = "SELECT * FROM `$tableName` WHERE" . implode(' OR', $clauses) . ' LIMIT ' . $offset . ',' . $limit;
    $sqlToSelectTotalCount = "SELECT COUNT(*) FROM `$tableName` WHERE" . implode(' OR', $clauses);
    $sqlToSelectRowsCount = "SELECT COUNT(*) FROM `$tableName`";
    unset($clauses);

// Select rows count
    $rowsCount = (int)$pdo->query($sqlToSelectRowsCount)->fetchColumn(0);

// Select total search result count
    $totalCount = (int)$pdo->query($sqlToSelectTotalCount)->fetchColumn(0);
    $pagesCount = ceil($totalCount / $limit);

// Select data
    $statement = $pdo->prepare($sqlToSelectData);
    $statement->execute();
    if ($statement->rowCount() > 0) {
        $rows = $statement->fetchAll();
        $exclude = ['ID', 'ItemID'];
        foreach ($rows as $index => &$r) {
            foreach ($r as $field => $value) {
                if (in_array($field, $exclude)) {
                    continue;
                }

                if (!preg_match('/[а-я]+/u', $value)) {
                    unset($r[$field]);
                }
            }
            if (
                count($r) == count($exclude)
                    && !count(
                    array_diff(
                        array_keys($r), $exclude
                    )
                )
            ) {
                unset($r[$index]);
                $totalCount--;
            }
        }
    } else {
        $rows = [];
    }
}
require_once __DIR__ . '/checker_view.php';