<?php

require_once __DIR__ . '/args.php';
require_once __DIR__ . '/vendor/autoload.php';

use Ychuperka\PhonesParser\Storage\ItemStorage;
use Ychuperka\PhonesParser\Cache\FileCache;
use Ychuperka\PhonesParser\Proxy\Manager;
use Ychuperka\PhonesParser\Proxy\Provider\HidemeApi;
use Onoi\HttpRequest\HttpRequestFactory;
use Sunra\PhpSimple\HtmlDomParser;

$targetData = $config['targets'][TARGET];

$itemStorage = new ItemStorage(
    $config['db'], $targetData['table_translated']
);
$cache = new FileCache(__DIR__ . '/amazon_rating_cache.bin');
$proxyManager = new Manager(
    new HidemeApi(
        '1024208353',
        [
            'maxtime' => 2000,
            'anon' => 34,
        ]
    )
);


$offset = 0;
$limit = $config['review_finder']['limit'];
for (; ;) {

    echo 'Selecting items, offset = ' . $offset, ', limit = ' . $limit . ' ...' . PHP_EOL;
    $list = $itemStorage->getList($offset, $limit);
    if (count($list) == 0) {
        break;
    }
    $offset += $limit;

    $listItemsCount = count($list);
    for ($i = 0; $i < $listItemsCount; ++$i) {

        $item = $list[$i];
        if ($item['AmazonRating']) {
            continue;
        }

        /*
         * Search for products
         */
        $amazonQuery = $item['Brand'] . ' ' . $item['Model'];
        // Try to find product rating in a cache
        if ($cache->exists($amazonQuery)) {

            echo 'A result for "' . $amazonQuery . '" is found in a cache.' . PHP_EOL;

            // Update item data
            $rating = $cache->get($amazonQuery);
            echo 'Rating is "' . $rating . '"' . PHP_EOL;

            $itemStorage->getPdo()
                ->exec("UPDATE `{$targetData['table_translated']}` SET `AmazonRating` = '{$rating}' WHERE `ID` = {$item['ID']}");
            echo 'Item data is updated.' . PHP_EOL;

            continue;
        }

        // Make request to the Amazon
        $requestParams = [
            'field-keywords' => $amazonQuery,
            'ie' => 'UTF-8',
            'rh' => str_replace('{keywords}', $amazonQuery, $targetData['amazon_category_data']['rh']),
            'fst' => $targetData['amazon_category_data']['fst'],
        ];
        $url = 'http://amazon.com/s/s/ref=sr_nr_n_5?' . http_build_query($requestParams);
        echo 'Querying Amazon for products, query is: ' . $amazonQuery . PHP_EOL
            . 'Search url: ' . $url . PHP_EOL;
        $request = (new HttpRequestFactory())->newCurlRequest('http://amazon.com');

        $cookiePath = __DIR__ . '/cookies_amazon.txt';
        if (file_exists($cookiePath)) {
            unlink($cookiePath);
        }

        if (isset($proxy)) {
            $proxyManager->release([$proxy]);
        }
        $proxyType = null;
        for (; ;) {
            $proxy = $proxyManager->getProxy();
            echo 'Obtained proxy. IP: ' . $proxy->ip . ' , port: ' . $proxy->port . ' , country: ' . $proxy->country_name . PHP_EOL;
            if ($proxy->http || $proxy->ssl) {
                $proxyType = CURLPROXY_HTTP;
            } else if ($proxy->socks4) {
                $proxyType = CURLPROXY_SOCKS4;
            } else if ($proxy->socks5) {
                $proxyType = CURLPROXY_SOCKS5;
            } else {
                echo 'Can`t determine proxy type, trying to use other...' . PHP_EOL;
                $proxyManager->release([$proxy]);
                continue;
            }
            break;
        }

        $options = [
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_COOKIEFILE => $cookiePath,
            CURLOPT_COOKIEJAR => $cookiePath,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36',
            CURLOPT_PROXY => $proxy->ip,
            CURLOPT_PROXYPORT => $proxy->port,
            CURLOPT_PROXYTYPE => $proxyType,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_SSL_VERIFYPEER => false,
        ];
        foreach ($options as $k => $v) {
            $request->setOption($k, $v);
        }

        $request->setOption(CURLOPT_URL, $url);
        $response = $request->execute();
        if ($request->getLastErrorCode() !== CURLE_OK) {
            echo 'Error! Can`t request products list, reason: ' . $request->getLastError() . ', code: '
                . $request->getLastErrorCode() . PHP_EOL;
            --$i; // Should try to search for the item again
            $proxyManager->remove($proxy);
            continue;
        }

        // Check dns resolution error
        if (stripos($response, 'dns resolution error')) {
            echo 'DNS resolution error! Trying again...' . PHP_EOL;
            $i--;
            $proxyManager->remove($proxy);
            continue;
        }

        // Check result empty
        if (strpos($response, 'did not match any products') !== false) {
            echo 'Empty search result!' . PHP_EOL;
            continue;
        }

        // Parse html into a dom representation
        $dom = HtmlDomParser::str_get_html($response);
        if (!$dom) {
            echo 'Can`t parse response!' . PHP_EOL;
            continue;
        }

        // Search for item containers
        $containers = $dom->find('div.s-item-container');
        if (count($containers) == 0) {
            echo 'Empty items list! May be banned... Trying again.' . PHP_EOL;
            --$i; // Should try to search for the item again
            $proxyManager->remove($proxy);
            continue;
        }
        $rating = null;
        foreach ($containers as $cn) {

            $anchor = $cn->find('a.s-access-detail-page', 0);
            if (!$anchor) {
                echo 'Can`t find anchor!' . PHP_EOL;
                continue;
            }

            $title = $anchor->plaintext;
            if (strlen($title) == 0) {
                echo 'Empty title!' . PHP_EOL;
                continue;
            }
            echo 'Processing item "' . $title . '"...' . PHP_EOL;

            // Check for bad words
            $badWords = [
                'case', 'cover', 'protector', ' for',
                'digitizer', 'keyboard', 'replacement',
                'charger',
            ];
            foreach ($badWords as $bw) {
                if (stripos($title, $bw) !== false) {
                    echo 'Found bad word "' . $bw . '" in a title "' . $title . '"!' . PHP_EOL;
                    continue 2;
                }
            }

            // Check Brand or Model part exists in a title
            $validItem = false;
            if (stripos($title, $item['Brand']) !== false) {
                $validItem = true;
            }
            if (!$validItem) {
                $parts = explode(' ', $item['Model']);
                foreach ($parts as $p) {
                    if (stripos($title, $p) !== false) {
                        $validItem = true;
                        break;
                    }
                }
            }
            if (!$validItem) {
                echo 'The item "' . $title . '" is invalid!' . PHP_EOL;
                continue;
            }

            // Get rating
            $ratingBlock = $cn->find('i.a-icon-star span.a-icon-alt', 0);
            if (!$ratingBlock) {
                echo 'Rating block is not found!' . PHP_EOL;
                continue;
            }
            $rating = (float)$ratingBlock->plaintext;

            $cache->set($amazonQuery, $rating);
            echo 'Rating is "' . $rating . '"' . PHP_EOL;

            // Update item data
            $itemStorage->getPdo()
                ->exec("UPDATE `{$targetData['table_translated']}` SET `AmazonRating` = '{$rating}' WHERE `ID` = {$item['ID']}");
            echo 'Item data is updated.' . PHP_EOL;

            break;
        }

    }
}

echo 'Done.' . PHP_EOL;