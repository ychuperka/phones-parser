<?php

return [

    'parser_class' => 'Ychuperka\\PhonesParser\\Parser\\Parser',
    'table' => 'tablets_ru',
    'table_translated' => 'tablets',
    'full_name_map' => ['Brand', 'Model'],
    'enter_url' => 'http://hotline.ua/computer/planshety/',
    'images_directory' => __DIR__ . '/../imgs/goods/tablets',
    'bad_url_signatures' => [
        'ua.hit.gemius.pl'
    ],
    'replace_rules' => [
        [
            'type' => 'after',
            'function' => 'preg_replace',
            'pattern' => '/^([Tt]here|[Tt]here\sare|[Hh]as)$/',
            'to' => 'yes',
            'fields' => '*',
        ],
        [
            'type' => 'before',
            'function' => 'preg_replace',
            'pattern' => '/(\d+)([[:alpha:]]+)/u',
            'to' => '$1 $2',
            'fields' => [
                'ScreenInfo',
                'Battery_life',
            ],
        ],
        [
            'type' => 'after',
            'function' => 'preg_replace',
            'pattern' => '/(\d+)\sh/',
            'to' => '$1h',
            'fields' => '*',
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => ['f/s', 'fps/s'],
            'to' => 'fps',
            'fields' => '*',
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => ['3 G', '3 g'],
            'to' => ['3G', '3g'],
            'fields' => '*',
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => 'a-GPS',
            'to' => 'A-GPS',
            'fields' => '*',
        ],
        [
            'type' => 'after',
            'function' => 'preg_replace',
            'pattern' => '/(\d+)х([A-z]+)/',
            'to' => '$1x$2',
            'fields' => [
                'CoresType',
            ]
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => 'GГГц',
            'to' => 'GHz',
            'fields' => ['Wi-Fi'],
        ],
    ],
    'ignore_translate' => [
        'ID', 'ItemID',
    ],

    'groups_map' => [
        'Оснащение' => 'common',
        'Характеристики экрана' => 'screen',
        'Характеристики процессора' => 'processor',
        'Операционная система' => 'os',
        'Коммуникации' => 'communication',
        'Батарея' => 'battery',
        'Физические параметры' => 'body',
        'Дополнительно' => 'additional',
    ],

    'attributes_map' => [
        'main' => [
            'Производитель' => 'Brand',
            'Модель из линейки' => 'Model',
            'Тип' => 'Type',
        ],

        'common' => [
            'Объем оперативной памяти, ГБ' => 'RAM',
            'Объем встроенной памяти, ГБ' => 'Memory',
            'Внешние порты' => 'Ports',
            'Картридер' => 'CardReader',
            'Фронтальная камера' => 'FrontCamera',
            'Тыловая камера' => 'MainCamera',
            'Датчик ориентации' => 'OrientationSensor',
            'Встроенные динамики' => 'Speakers',
            'Док-станция' => 'Docking',
            'Стилус в комплекте' => 'Stylus',
        ],

        'screen' => [
            'Диагональ экрана, дюймов' => 'ScreenSize',
            'Матрица' => 'ScreenType',
            'Тип покрытия экрана' => 'CoatingType',
            'Разрешение экрана' => 'Resolution',
            'Тип сенсорной панели' => 'ScreenInfo',
            'Multi touch' => 'ScreenInfo',
            'Другое' => 'ScreenInfo',
        ],

        'processor' => [
            'Процессор' => 'Processor',
            'Тип ядра' => 'CoresType',
            'Количество ядер' => 'AmountOfCores',
            'Частота, ГГц' => 'Frequency',
            'Графика' => 'GPU',
        ],

        'os' => [
            'Предустановленная ОС' => 'OS',
        ],

        'communication' => [
            'Ethernet' => 'Ethernet',
            'Wi-Fi' => 'Wi-Fi',
            'Bluetooth' => 'Bluetooth',
            'Модуль 3G/4G(LTE)' => '3G_Module',
            'Стандарты GSM/3G/4G(LTE)' => 'NetworkStandards',
            'Голосовая связь в GSM/3G сетях' => 'VoiceCalls',
            'GPS' => 'GPS',
            'NFC' => 'NFC',
        ],

        'battery' => [
            'Емкость батареи, мАч' => 'Battery_capacity',
            'Время автономной работы' => 'Battery_life',
        ],

        'body' => [
            'Вес, г' => 'Weight',
            'Размеры, мм' => 'Sizes',
        ],

        'additional' => [
            'Прочее' => 'Other',
            'Цвет корпуса' => 'BodyColor',
            'Цвет лицевой панели' => 'FrontPanelColor',
            'Товар на сайте производителя' => 'ManufacturerLink',
        ],
    ],

    'checker_fields' => [
        'Brand', 'Model', 'Type', 'RAM', 'Memory', 'Ports', 'CardReader', 'FrontCamera',
        'MainCamera', 'OrientationSensor', 'Speakers', 'Docking', 'Stylus', 'ScreenSize',
        'ScreenType', 'CoatingType', 'Resolution', 'ScreenInfo', 'ScreenInfo', 'ScreenInfo', 'Processor',
        'CoresType', 'AmountOfCores', 'Frequency', 'GPU', 'OS', 'Ethernet', 'Wi-Fi', 'Bluetooth', '3G_Module',
        'NetworkStandards', 'VoiceCalls', 'GPS', 'NFC', 'Battery_capacity', 'Battery_life', 'Other', 'BodyColor',
        'FrontPanelColor', 'Weight', 'Sizes',
    ],

    'amazon_category_data' => [
        'qid' => '1446805816',
        'rnid' => '493964',
        'rh' => 'n:172282,n:1232597011,k:{keywords}',
        'fst' => 'as:off',
    ],
];