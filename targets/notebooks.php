<?php

return [

    'parser_class' => 'Ychuperka\\PhonesParser\\Parser\\Parser',
    'table' => 'notebooks_ru',
    'table_translated' => 'notebooks',
    'full_name_map' => ['Brand', 'Model'],
    'enter_url' => 'http://hotline.ua/computer/noutbuki-netbuki/',
    'images_directory' => __DIR__ . '/../imgs/goods/notebooks',
    'bad_url_signatures' => [],
    'replace_rules' => [
        [
            'type' => 'after',
            'function' => 'preg_replace',
            'pattern' => '/^([Tt]here|[Tt]here\sare|[Hh]as)$/',
            'to' => 'yes',
            'fields' => '*',
        ],
        [
            'type' => 'before',
            'function' => 'preg_replace',
            'pattern' => '/(Ноутбук|Ультрабук)\s+/u',
            'to' => null,
            'fields' => [
                'Model',
            ],
        ],
        [
            'type' => 'after',
            'function' => 'preg_replace',
            'pattern' => '/(\d+)х([A-z]+)/',
            'to' => '$1x$2',
            'fields' => [
                'Ports',
            ]
        ],
    ],
    'ignore_translate' => [
        'ID', 'ItemID', 'ManufacturerLink',
    ],

    'groups_map' => [
        'Характеристики экрана' => 'screen',
        'Процессор' => 'processor',
        'Операционная система' => 'OS',
        'Оснащение' => 'equipment',
        'Коммуникации' => 'communication',
        'Физические параметры' => 'body',
        'Батарея' => 'battery',
    ],

    'attributes_map' => [
        'main' => [
            'Производитель' => 'Brand',
            'Модель из линейки' => 'Model',
            'Класс' => 'Class',
            'Конструкция' => 'Construction',
        ],

        'screen' => [
            'Диагональ экрана, дюймов' => 'ScreenSize',
            'Разрешение экрана' => 'Resolution',
            'Тип матрицы' => 'ScreenType',
            'Тип покрытия экрана' => 'ScreenCoating',
            'Сенсорный экран' => 'ScreenSensor',
        ],

        'processor' => [
            'Тип процессора' => 'Processor',
            'Количество ядер процессора' => 'AmountOfCores',
            'Частота, ГГц' => 'Frequency',
        ],

        'OS' => [
            'Предустановленная ОС' => 'OS',
        ],

        'equipment' => [
            'Чипсет' => 'ChipSet',
            'Объем оперативной памяти, ГБ' => 'RAM',
            'Максимальный объем оперативной памяти, ГБ' => 'MaxRAM',
            'Жесткий диск, ГБ' => 'HDD',
            'SSD, ГБ' => 'SSD',
            'Оптический привод' => 'OpticalDrive',
            'Графический адаптер, объем памяти' => 'VideoCard',
            'Внешние порты' => 'Ports',
            'Слот расширения' => 'ExtensionSlot',
            'Кардридер' => 'CardReader',
            'WEB-камера' => 'WebCam',
            'Подсветка клавиатуры' => 'KeyboardLight',
        ],

        'communication' => [
            'Сетевой адаптер' => 'NetworkAdapter',
            'Wi-Fi' => 'Wi-Fi',
            'Bluetooth' => 'Bluetooth',
            '3G' => '3G',
        ],

        'body' => [
            'Вес, кг' => 'Weight',
            'Размер, мм' => 'Sizes',
            'Материал корпуса' => 'BodyMaterial',
            'Цвет корпуса' => 'BodyColor',
        ],

        'battery' => [
            'Емкость, мАч' => 'BatteryCapacity',
            'Количество ячеек' => 'BatteryCellsAmount',
            'Мощность, Вт*ч' => 'BatteryPower',
            'Напряжение батареи, В' => 'BatteryVoltage',
            'Тип аккумулятора' => 'BatteryType',
            'Товар на сайте производителя' => 'ManufacturerLink',
        ],
    ],

    'checker_fields' => [
        'Brand', 'Model', 'Class', 'Construction', 'ScreenSize',
        'Resolution', 'ScreenType', 'ScreenCoating', 'ScreenSensor', 'Processor',
        'AmountOfCores', 'Frequency', 'OS', 'ChipSet', 'RAM', 'MaxRAM',
        'HDD', 'SSD', 'OpticalDrive', 'Videocard', 'Ports', 'ExtensionSlot',
        'CardReader', 'WebCam', 'KeyboardLight', 'NetworkAdapter', 'Wi-Fi', '3G',
        'Bluetooth', 'Weight', 'Sizes', 'BodyMaterial', 'BodyColor', 'BatteryCapacity',
        'BatteryCellsAmount', 'BatteryPower', 'BatteryVoltage', 'BatteryType',

    ],

    'amazon_category_data' => [
        'qid' => '1446806568',
        'rnid' => '2941120011',
        'rh' => 'n:172282,n:565108,k:{keywords}',
        'fst' => 'as:off',
    ],
];