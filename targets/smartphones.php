<?php

return [

    'parser_class' => 'Ychuperka\\PhonesParser\\Parser\\Parser',
    'table' => 'phones_ru',
    'table_translated' => 'phones',
    'full_name_map' => ['Brand', 'Model'],
    'enter_url' => 'http://hotline.ua/mobile/mobilnye-telefony-i-smartfony/',
    'images_directory' => __DIR__ . '/../imgs/goods/phones',
    'bad_url_signatures' => [
        'ua.hit.gemius.pl'
    ],
    'replace_rules' => [
        [
            'type' => 'after',
            'function' => 'preg_replace',
            'pattern' => '/^([Tt]here|[Tt]here\sare|[Hh]as)$/',
            'to' => 'yes',
            'fields' => '*',
        ],
        [
            'type' => 'before',
            'function' => 'str_replace',
            'pattern' => ['разговор - до', 'ожидание - до'],
            'to' => ['разговор до', 'ожидание до'],
            'fields' => [
                'Battery_life'
            ],
        ],
        [
            'type' => 'before',
            'function' => 'preg_replace',
            'pattern' => '/(\d+)([[:alpha:]]+)/u',
            'to' => '$1 $2',
            'fields' => [
                'ScreenInfo',
                'Battery_life',
            ],
        ],
        [
            'type' => 'after',
            'function' => 'preg_replace',
            'pattern' => '/(\d+)\sh/',
            'to' => '$1h',
            'fields' => '*',
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => ['f/s', 'fps/s'],
            'to' => 'fps',
            'fields' => '*',
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => ['3 G', '3 g'],
            'to' => ['3G', '3g'],
            'fields' => '*',
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => 'a-GPS',
            'to' => 'A-GPS',
            'fields' => '*',
        ],
        [
            'type' => 'after',
            'function' => 'preg_replace',
            'pattern' => '/(\d+)х([A-z]+)/',
            'to' => '$1x$2',
            'fields' => [
                'CoresType',
            ]
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => 'GГГц',
            'to' => 'GHz',
            'fields' => ['Wi-Fi'],
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => 'обеспечиваtn ',
            'to' => null,
            'fields' => ['Extra'],
        ],
        [
            'type' => 'after',
            'function' => 'str_replace',
            'pattern' => 'In-сell',
            'to' => 'In-cell',
            'fields' => ['ScreenInfo'],
        ],
    ],
    'ignore_translate' => [
        'ID', 'ItemID',
    ],

    'groups_map' => [
        'Общие характеристики' => 'common',
        'Экран' => 'screen',
        'Характеристики процессора' => 'processor',
        'Камера' => 'camera',
        'Коммуникации' => 'communication',
        'Дополнительно' => 'additional',
    ],

    'attributes_map' => [
        'main' => [
            'Производитель' => 'Brand',
            'Модель из линейки' => 'Model',
            'Тип' => 'Type',
            'Тип SIM-карты' => 'SimCard',
        ],

        'common' => [
            'Стандарт' => 'Connection',
            'Высокоскоростная передача данных' => 'Connection',
            'Количество SIM-карт' => 'SimCardAmount',
            'Операционная система' => 'OS',
            'Оперативная память, ГБ' => 'RAM',
            'Встроенная память, ГБ' => 'Memory',
            'Слот расширения' => 'SD_slot',
            'Размеры, мм' => 'Sizes',
            'Вес, г' => 'Weight',
            'Защита от пыли и влаги' => 'Protection',
            'Аккумуляторная батарея' => 'Battery',
            'Время работы (данные производителя)' => 'Battery_life',
        ],

        'screen' => [
            'Диагональ, дюймы' => 'ScreenSize',
            'Разрешение' => 'Resolution',
            'Тип матрицы' => 'ScreenType',
            'PPI' => 'PPI',
            'Датчик регулировки яркости' => 'DimmingSensor',
            'Сенсорный экран (тип)' => 'ScreenInfo',
            'Другое' => 'ScreenInfo',
        ],

        'processor' => [
            'Процессор' => 'Processor',
            'Тип ядра' => 'CoresType',
            'Количество ядер' => 'AmountOfCores',
            'Частота, ГГц' => 'Frequency',
        ],

        'camera' => [
            'Основная камера, МП' => 'MainCamera',
            'Автофокус' => 'Autofocus',
            'Видеосъемка' => 'Video',
            'Вспышка' => 'Flash',
            'Фронтальная камера, МП' => 'FrontCamera',
            'Другое' => 'CameraInfo'
        ],

        'communication' => [
            'Wi-Fi' => 'Wi-Fi',
            'Bluetooth' => 'Bluetooth',
            'GPS' => 'GPS',
            'IrDA' => 'IrDA',
            'NFC' => 'NFC',
            'Интерфейсный разъем' => 'Cable',
        ],

        'additional' => [
            'Аудиоразъем' => 'AudioJack',
            'FM-радио' => 'FM-radio',
            'Тип корпуса' => 'BodyType',
            'Материал корпуса' => 'BodyMaterial',
            'Тип клавиатуры' => 'Keyboard',
            'Еще' => 'Extra',
        ],
    ],

    'checker_fields' => [
        'Brand', 'Model', 'Type', 'SimCard', 'Connection',
        'SimCardAmount', 'OS', 'RAM', 'Memory', 'SD_slot',
        'Sizes', 'Weight', 'Protection', 'Battery', 'Battery_life',
        'ScreenSize', 'Resolution', 'ScreenType', 'PPI',
        'DimmingSensor', 'ScreenInfo', 'Processor', 'CoresType',
        'AmountOfCores', 'Frequency', 'MainCamera', 'Autofocus',
        'Video', 'Flash', 'FrontCamera', 'CameraInfo', 'Wi-Fi',
        'Bluetooth', 'GPS', 'IrDA', 'NFC', 'Cable', 'AudioJack',
        'FM-radio', 'BodyType', 'BodyMaterial', 'Keyboard',
        'Extra',
    ],

    'amazon_category_data' => [
        'qid' => '1446803392',
        'rnid' => '2335753011',
        'rh' => 'n:2335752011,n:2407749011,k:{keywords}',
        'fst' => 'as:off',
    ],
];