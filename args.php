<?php

if (php_sapi_name() != 'cli') {
    echo 'Sorry, only CLI mode is supported.' . PHP_EOL;
    exit(1);
}

if ($argc < 2) {
    echo 'Usage: php script.php target' . PHP_EOL;
    exit(1);
}

define('TARGET', $argv[1]);
$config = require_once(__DIR__ . '/config.php');
if (!isset($config['targets'][TARGET])) {
    echo 'A target "' . TARGET . '" is not found!' . PHP_EOL;
    echo 'Available targets:' . PHP_EOL;
    echo implode(PHP_EOL, array_keys($config['targets'])) . PHP_EOL;
    exit(1);
}
