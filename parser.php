<?php

require_once __DIR__ . '/args.php';
require_once __DIR__ . '/vendor/autoload.php';

use Onoi\HttpRequest\HttpRequestFactory;
use Ychuperka\PhonesParser\Storage\ItemStorage;
use Ychuperka\PhonesParser\Storage\ImageStorage;
use Ychuperka\PhonesParser\Parser\Exception as ParserException;

$targetData = $config['targets'][TARGET];

$itemStorage = new ItemStorage($config['db'], $targetData['table']);
$imageStorage = new ImageStorage($targetData['images_directory']);

/** @var $parser \Ychuperka\PhonesParser\Parser\Parser */
$parser = new $targetData['parser_class'](
    $targetData['enter_url'], $targetData['groups_map'], $targetData['attributes_map'],
    './cookies.txt'
);
try {
    $parser->addUserAgent($config['parser']['user_agent'])
        ->setBadUrlSignatures($targetData['bad_url_signatures'])
        ->setIsItemExistsCallback(
            function ($itemId) use ($itemStorage) {
                return $itemStorage->exists($itemId);
            }
        )->loadItems(
            function (array $item) use ($config, $itemStorage, $imageStorage) {

                if (!isset($item['attributes']['Brand'])
                    || !isset($item['attributes']['Model'])
                ) {
                    echo 'ALERT! Brand, Model or Type is not found! Can not save the item.' . PHP_EOL;
                    return;
                }

                echo "Item data parsed, brand: {$item['attributes']['Brand']}, model: {$item['attributes']['Model']}...\n";

                if ($itemStorage->exists($item['attributes']['ItemID'])) {
                    echo 'Item already exists!' . PHP_EOL;
                    return;
                }


                echo 'Saving item...' . PHP_EOL;
                $rowId = $itemStorage->save($item['attributes']);
                if ($rowId === false) {
                    echo 'Can not save item!' . PHP_EOL;
                    exit(1);
                }
                echo 'OK! Row id: ' . $rowId . PHP_EOL;

                /*
                 * Download item images
                 */
                echo 'Processing item images...' . PHP_EOL;
                $imageCounter = 0;
                $factory = new HttpRequestFactory();
                foreach ($item['images'] as $i) {

                    $filename = $item['attributes']['ItemID'] . '_' . $imageCounter;
                    if ($imageStorage->imageExists($filename)) {
                        echo 'Image "' . $filename . '" already exists...' . PHP_EOL;
                        $imageCounter++;
                        continue;
                    }

                    echo 'Downloading image "' . $i . '"...' . PHP_EOL;
                    $curlHttpRequest = $factory->newCurlRequest();
                    $options = [
                        CURLOPT_COOKIEFILE => './cookies.txt',
                        CURLOPT_COOKIEJAR => './cookies.txt',
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $i,
                        CURLOPT_USERAGENT => $config['parser']['user_agent'],
                    ];
                    foreach ($options as $k => $o) {
                        $curlHttpRequest->setOption($k, $o);
                    }
                    $imageContents = $curlHttpRequest->execute();
                    if ($curlHttpRequest->getLastErrorCode() != CURLE_OK) {
                        echo 'Can not download image, curl error code #' . $curlHttpRequest->getLastErrorCode()
                            . ', message: ' . $curlHttpRequest->getLastError() . PHP_EOL;
                    }

                    echo 'Saving item image...' . PHP_EOL;
                    echo 'Filename: ' . $filename . PHP_EOL;
                    $imageStorage->saveImage($filename, $imageContents);
                    $imageCounter++;
                }

            }
        );
} catch (ParserException $e) {
    echo 'Parser exception: ' . $e->getMessage() . PHP_EOL;
    file_put_contents(__DIR__ . '/bad_response.txt', $e->getResponse());
} catch (\Exception $e) {
    echo 'ALERT! Exception occur: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString() . PHP_EOL;
    exit(1);
}