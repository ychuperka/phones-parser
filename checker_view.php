<!DOCTYPE html>
<html>
<head>
    <title>Translation Checker</title>
    <meta http-equiv="content-type" content="text/html; charset=utf8"/>
    <link rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/vendor/twbs/bootstrap/dist/css/bootstrap-theme.min.css"/>
    <script type="text/javascript" src="/vendor/components/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function() {
            var editIcons = $("i.field-edit");
            editIcons.css({cursor: "pointer"});
            editIcons.click(function(event) {

                var theIcon = $(this);
                theIcon.hide();

                var span = theIcon.prev();
                var value = span.html();
                var input = $("<input type=\"text\" class=\"form-control\" value=\"" + value + "\"/>");
                span.replaceWith(input);

                input.focus();
                input.on("focusout keypress", function(event) {


                    if (event.type != "focusout" && event.type != "keypress") {
                        return;
                    }
                    if (event.type == "keypress") {
                        var keyCode = (event.keyCode ? event.keyCode : event.which);
                        if (keyCode != '13') {
                            return;
                        }
                    }

                    var newValue = $(this).val();
                    if (newValue == value) {
                        input.replaceWith(span);
                        theIcon.show();
                        return;
                    }

                    var dataToSend = {
                        table: span.data("table"),
                        id: span.data("id"),
                        field: span.data("field"),
                        value: newValue
                    };

                    $.ajax({
                        url: "/value_update.php",
                        method: "POST",
                        data: dataToSend,
                        dataType: "text",
                        success: function (response) {

                            if (response != 'OK') {
                                alert('Can not update data!');
                                return;
                            }

                            input.replaceWith(span);
                            span.html(newValue);
                            theIcon.show();
                        },
                        error: function (response) {
                            alert(response.responseText);
                        }
                    });
                });

            });
        });
    </script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-md-offset-2">
            <h1>Translation Check</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form class="form-inline" action="" method="post" enctype="application/x-www-form-urlencoded">
                <div class="form-group">
                    <label class="control-label" for="table-name">Table Name:</label>
                    <input class="form-control" type="text" name="table_name" id="table-name"
                           value="<?= $tableName ?>"/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Check</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <br/>

        <div class="col-md-4 col-md-offset-2">
            <p>
                <b>Table rows count:</b> <span><?= $rowsCount; ?></span><br/>
                <b>Rows with cyrillic characters count:</b> <span><?= $totalCount; ?></span>
            </p>
        </div>
    </div>
    <?php if ($pagesCount > 1): ?>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <nav>
                    <ul class="pagination">
                        <?php for ($i = 0; $i < $pagesCount; ++$i): ?>
                            <li><a href="/checker.php?table_name=<?= $tableName ?>&page=<?= $i ?>"><?= $i + 1 ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </nav>
            </div>
        </div>
    <?php endif; ?>
    <?php if (count($rows) > 0): ?>
        <div class="row">
            <h2>Results</h2>

            <div class="col-xs-12">
                <?php foreach ($rows as $r): ?>
                    <div class="col-xs-4">
                        <div class="panel panel-default">
                            <span class="panel-body">
                                <ul class="list-group">
                                    <?php $id = null; ?>
                                    <?php foreach ($r as $field => $value): ?>
                                        <?php
                                        if ($field === 'ID') {
                                            $id = $value;
                                        }
                                        ?>
                                        <li class="list-group-item">
                                            <b><?= $field ?></b>: <span data-field="<?= $field ?>" data-table="<?= $tableName ?>" data-id="<?= $id ?>"><?= $value ?></span>
                                            <i class="field-edit glyphicon glyphicon-pencil"></i>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
</body>
</html>