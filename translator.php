<?php

require_once __DIR__ . '/args.php';
require_once __DIR__ . '/vendor/autoload.php';

use Ychuperka\PhonesParser\Cache\FileCache;
use Ychuperka\PhonesParser\Storage\ItemStorage;
use Ychuperka\PhonesParser\Translator\Translator;
use Ychuperka\PhonesParser\Translator\Exception as TranslatorException;
use Ychuperka\PhonesParser\Storage\Exception as StorageException;
use Ychuperka\PhonesParser\Cache\Exception as CacheException;

$targetData = $config['targets'][TARGET];

$translator = new Translator(
    new \Ychuperka\PhonesParser\Translator\Provider\YandexTranslate($config['translator']['yandex']['api_key'])
);
$cache = new FileCache(__DIR__ . '/translator_cache.bin');
$sourceItemStorage = new ItemStorage(
    $config['db'], $targetData['table']
);
$targetItemStorage = new ItemStorage(
    $config['db'], $targetData['table_translated']
);

$offset = 0;
$limit = $config['translator']['limit'];
try {
    for (; ;) {

        echo "Selecting items list, offset = $offset, limit = $limit...\n";
        $list = $sourceItemStorage->getList($offset, $limit);
        $offset += $limit;

        if (count($list) == 0) {
            break;
        }

        foreach ($list as $item) {

            if ($targetItemStorage->exists($item['ItemID'])) {
                continue;
            }

            echo 'Translating item ' . $item['ItemID'] . PHP_EOL;

            $itemTranslated = [];
            foreach ($item as $key => $value) {

                if (in_array($key, $targetData['ignore_translate'])) {
                    $itemTranslated[$key] = $item[$key];
                    continue;
                }

                foreach ($targetData['replace_rules'] as $rule) {
                    if ($rule['type'] != 'before') {
                        continue;
                    }
                    if ($rule['fields'] === '*'
                        || (is_array($rule['fields']) && in_array($key, $rule['fields']))) {
                        $old = $item[$key];
                        $item[$key] = $value = call_user_func($rule['function'], $rule['pattern'], $rule['to'], $item[$key]);
                        echo "\"$old\" is replaced by \"{$item[$key]}\"\n";
                    }
                }

                if (!in_array($key, $config['translator']['keys_to_exclude'])) {
                    if ($cache->exists($value)) {
                        $itemTranslated[$key] = $cache->get($value);
                    } else {
                        $itemTranslated[$key] = $translator->translate($value);
                        $cache->set($value, $itemTranslated[$key]);
                    }
                } else {
                    $itemTranslated[$key] = $value;
                }

                foreach ($targetData['replace_rules'] as $rule) {
                    if ($rule['type'] != 'after') {
                        continue;
                    }
                    if ($rule['fields'] === '*'
                        || (is_array($rule['fields']) && in_array($key, $rule['fields']))) {
                        $old = $itemTranslated[$key];
                        $itemTranslated[$key] = call_user_func(
                            $rule['function'], $rule['pattern'], $rule['to'], $itemTranslated[$key]
                        );
                        echo "\"$old\" is replaced by \"{$itemTranslated[$key]}\"\n";
                    }
                }

            }

            echo 'Saving...' . PHP_EOL;
            unset($itemTranslated['ID']);
            $rowId = $targetItemStorage->save($itemTranslated);
            echo 'OK! Row ID: ' . $rowId . PHP_EOL;
        }

    }
} catch (TranslatorException $e) {
    echo 'Translator exception occur: ' . $e->getMessage() . PHP_EOL;
} catch (StorageException $e) {
    echo 'Storage exception occur: ' . $e->getMessage() . PHP_EOL;
} catch (CacheException $e) {
    echo 'Cache exception occur: ' . $e->getMessage() . PHP_EOL;
} catch (\Exception $e) {
    echo 'Unknown exception occur: ' . $e->getMessage() . PHP_EOL;
}

echo 'Done.' . PHP_EOL;

